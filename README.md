# RNG Contact

Provides a non-user contact suitable for usage with [RNG](https://drupal.org/project/rng).
